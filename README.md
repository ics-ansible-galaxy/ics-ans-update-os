# ics-ans-update-os

Ansible playbook to update the OS:
- update all packages
- reboot the server if required
- re-install the NVIDIA drivers if a NVIDIA card is detected (using lspci)

Only CentOS is supported so far.

WARNING! The playbook is applied to all hosts. You should use limit to apply it to the wanted machines.

## Requirements

- ansible >= 2.7
- molecule >= 2.6

## License

BSD 2-clause
