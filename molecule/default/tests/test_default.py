import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_centos_release(host):
    assert host.file("/etc/centos-release").contains("CentOS Linux release 7.8.2003")
